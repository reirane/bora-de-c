/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
 * The C programming language. 
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 2-4. Write an alternate version of squeeze(s1, s2) that deletes
 * each character in s1 that matches any character in the string s2.
 * 
 * reirane e ewerton
 * 18.06.2020
 * 
 * input: ""
 * 
 * output: "bcdffghhj"
 * 
 */

#include<stdio.h>

/* squeeze: delete all characters in s2 from s1 */
void squeeze_r(char[], char[], char[]);
void squeeze_e(char[], char[], char[]);

int main() {
    char result_r[100];
    char result_e[100];
    
    squeeze_r("abcdeffghihj", "aeiou", result_r);
    squeeze_e("abcdeffghihj", "aeiou", result_e);
    
    printf("%s\n", result_r);
    printf("%s", result_e);
    
    return 0;
}

void squeeze_r(char s1[], char s2[], char result[])
{
    int index_s1, index_s2, index_result;
    
    index_s1 = index_s2 = index_result = 0;
    
    while (s1[index_s1] != '\0')
        if (s1[index_s1] == s2[index_s2]) {
            index_s1++;
            index_s2++;
        }
        
        else
            if (s2[index_s2 + 1] == '\0') {
                result[index_result++] = s1[index_s1++];
                index_s2 = 0; 
            }
            
            else
                index_s2++;
        
    result[index_result] = '\0';
}

void squeeze_e(char s1[], char s2[], char result[])
{
    int index_s1, index_s2, index_result, equal;
    
    index_result = equal = 0;
    
    for (int index_s1 = 0; s1[index_s1] != '\0'; index_s1++) {
        for (int index_s2 = 0; s2[index_s2] != '\0'; index_s2++) {
            if (s1[index_s1] == s2[index_s2]) {
                equal = 1;
                break;
            }
        }
        
        if (!equal)
            result[index_result++] = s1[index_s1];
    
        equal = 0;
    }
    
    result[index_result] = '\0';
}

