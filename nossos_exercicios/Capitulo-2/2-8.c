#include<stdio.h>
#include<limits.h>

/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E.
 * The C programming language.
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 2-8. Write a function rightrot(x,n) that returns
 * the value of the integer x rotated to the right by n bit
 * positions.
 *
 * reirane e ewerton
 * 27.07.2020
 *
 * input: ""
 *
 * output: "222"
 *         "159"
 *         "222"
 *         "162"
 *         "59"
 *         "233"
 *         "253"
 *         "222"
 *         "47"
 *         "193"
 *
 */

/**
 * This function waits for a bit chain of eight elements
 * and an arbitrary n. In our tests, we had problems with
 * bit chains bigger than 8.
 */
unsigned rightrot(unsigned, int);

int main()
{
    printf("%u\n", rightrot(0b10110111, 6));
    printf("%u\n", rightrot(0b11001111, 7));
    printf("%u\n", rightrot(0b10111101, 1));
    printf("%u\n", rightrot(0b10100010, 8));
    printf("%u\n", rightrot(0b11011001, 11));
    printf("%u\n", rightrot(0b01111010, 22));
    printf("%u\n", rightrot(0b11111110, 23));
    printf("%u\n", rightrot(0b10111101, 17));
    printf("%u\n", rightrot(0b01111001, 3));
    printf("%u\n", rightrot(0b00000111, 2));
}

unsigned rightrot(unsigned x, int n)
{
    /**/
    int x_mask = ~0;
    x_mask <<= n % CHAR_BIT;
    x_mask = ~x_mask;

    int removed_from_x = x & x_mask;
    removed_from_x <<= CHAR_BIT - (n % CHAR_BIT);
    int new_x = x >> n % CHAR_BIT;

    return new_x | removed_from_x;
    /**/

    /** /
    int x_mask = ~(~0 << n % CHAR_BIT);
    int removed_from_x = (x & x_mask) << CHAR_BIT - (n % CHAR_BIT);
    int new_x = x >> n % CHAR_BIT;

    return new_x | removed_from_x;
    /**/

    /** /
    return ((x & ~(~0 << n % CHAR_BIT)) << (CHAR_BIT - (n % CHAR_BIT))) | (x >> (n % CHAR_BIT));
    /**/
}
