/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E.
 * The C programming language.
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 2-6. Escreva uma funçao setbit(x, p, n, y) que devolve x com n bits
 * modificados de acordo com os n bits menos significativos de y, onde a contagem
 * dos n bits de x inicia em p. Os bits restantes de x não devem ser modificados.
 *
 * reirane e ewerton
 * 13.07.2020
 *
 * input: ""
 *
 * output: "95"
 *         "159"
 *         "95"
 *         "159"
 *
 */

#include<stdio.h>

unsigned setbits_r (unsigned, int, int, unsigned);
unsigned setbits_e (unsigned, int, int, unsigned);

int main()
{
    printf("%d\n", setbits_r(0b01001111, 5, 3, 0b11101101));
    printf("%d\n", setbits_r(191, 5, 3, 153));
    printf("%d\n", setbits_e(0b01001111, 5, 3, 0b11101101));
    printf("%d\n", setbits_e(191, 5, 3, 153));
}

unsigned setbits_r(unsigned x, int p, int n, unsigned y)
{
    /* expressão dividida em passos: * /
    int passo1, passo2, passo3, passo4, passo5, passo6, passo7, passo8, passo9;
    
    passo1 = ~(~0 << n);                
    passo2 = passo1 & y; 
    passo3 = passo2 << p; 
    passo4 = ~passo3;  
    passo5 = ~(~0 << n); 
    passo6 = passo5 << p;
    passo7 = ~passo6;
    passo8 = passo7 & x;
    passo9 = ~(~passo8 & passo4);
    
    return passo9;
    /**/
    
    /** /
    int r, ymask, xmask;
    
    xmask = (~((~((~(~0 << n)) << p)) & x));
    ymask = (~(((~(~0 << n)) & y) << p));
    r = ~(xmask & ymask);
    
    return r;
    /**/

    /**/
    return ~((~((~((~(~0 << n)) << p)) & x)) & (~(((~(~0 << n)) & y) << p)));
    /**/
}

/*
unsigned setbits_r (0100 1111, 5, 3, 1110 1101)
    passo1 = ~(~0 << n);            // passo1 = -> 1111 1000 -> 0000 0111
    passo2 = 0000 0111 & y;         // passo2 = -> 0000 0111 & 1110 1101 -> 0000 0101
    passo3 = 0000 0001 << p;        // passo3 = -> 1010 0000
    passo4 = ~(0010 0000));         // passo4 = -> 0101 1111
    passo5 = ~(~0 << n);            // passo5 = -> 1111 1000 -> 0000 0111 
                                    // preparacao da mascara de x
    passo6 = 0000 0111 << p;        // passo6 = 1110 0000 
                                    // colocacao de n bits verdadeiros na posicao p
    passo7 = ~(1110 0000);          // passo7 = 0001 1111 
                                    // negação da máscara de modo que zeremos os n bits de interesse, q os tornemos falsos
    passo8 = 0001 1111 & x;         // passo8 = 0001 1111 & 1110 1101 -> 0000 1101
                                    // operacao que zera os n bits na posicao p em x
    passo9 = ~(~passo8 & passo4)    // passo9 = 1111 0010 & 0101 1111 -> 0101 0010 -> 1010 1101 
                                    // conjunção da máscara e x invertidos para incorporar n bits em p em x e posterior 
                                    // negacao para resgatar valores originais de x e dos n bits

--

unsigned setbits_r (1011 1111, 5, 3, 1001 1001)
    passo1 = ~(~0 << n);            // passo1 = -> 1111 1000 -> 0000 0111
    passo2 = 0000 0111 & y;         // passo2 = -> 0000 0111 & 1001 1001 -> 0000 0001
    passo3 = 0000 0001 << p;        // passo3 = -> 0010 0000
    passo4 = ~(0010 0000));         // passo4 = -> 1101 1111
    passo5 = ~(~0 << n);            // passo5 = -> 1111 1000 -> 0000 0111 
                                    // preparacao da mascara de x
    passo6 = 0000 0111 << p;        // passo6 = 1110 0000 
                                    // colocacao de n bits verdadeiros na posicao p
    passo7 = ~(1110 0000);          // passo7 = 0001 1111 
                                    // negacao da mascara de modo que zeremos os n bits de interesse, q os tornemos falsos
    passo8 = 0001 1111 & x;         // passo8 = 0001 1111 & 1011 1111 -> 0001 1111
                                    // operacao que zera os n bits na posicao p em x
    passo9 = ~(~passo8 & passo4)    // passo9 = 1110 0000 & 1101 1111 -> ~1100 0000 -> 0011 1111 
                                    // conjuncao da máscara e x invertidos para incorporar n bits em p em x e posterior 
                                    // negacao para resgatar valores originais de x e dos n bits
*/

unsigned setbits_e (unsigned x, int p, int n, unsigned y)
{
    /**/
    int y_mask = ~0;
    y_mask <<= n;
    y_mask = ~y_mask;

    int n_y = y & y_mask;
    n_y <<= p - 1;

    int x_mask = ~0;
    x_mask <<= n;
    x_mask = ~x_mask;
    x_mask <<= p - 1;
    x_mask = ~x_mask;

    int n_x = x & x_mask;

    return n_x | n_y;
    /**/

    /** /
    int n_y, n_x;

    n_y = (y & ~(~0 << n)) << p - 1;

    n_x = x & ~(~(~0 << n) << p - 1);

    return n_x | n_y;
    /**/

    /** /
    return (x & ~(~(~0 << n) << p - 1)) | ((y & ~(~0 << n)) << p - 1);
    /**/
}

/*
unsigned setbits_e (0100 1111, 5, 3, 1110 1101)
    y_mask = ~0;                // y_mask = 1111 1111
    y_mask <<= n;               // y_mask << 3 = 1111 1000
    y_mask = ~y_mask;           // ~y_mask = 0000 0111

    n_y = y & y_mask;           // n_y = 1110 1101 & 0000 0111 = 0000 0101
    n_y <<= p - 1;              // n_y << 4 = 0101 0000

    x_mask = ~0;                // x_mask = 1111 1111
    x_mask <<= n;               // x_mask << 3 = 1111 1000
    x_mask = ~x_mask;           // ~x_mask = 0000 0111
    x_mask <<= p - 1;           // x_mask << 4 = 0111 0000
    x_mask = ~x_mask;           // ~x_mask = 1000 1111

    n_x = x & x_mask;           // n_x = 0100 1111 & 1000 1111 = 0000 1111

    return n_x | n_y;           // 0000 1111 | 0101 0000 = 0101 1111

--

unsigned setbits_e (1011 1111, 5, 3, 1001 1001)
    y_mask = ~0;                // y_mask = 1111 1111
    y_mask <<= n;               // y_mask << 3 = 1111 1000
    y_mask = ~y_mask;           // ~y_mask = 0000 0111

    n_y = y & y_mask;           // n_y = 1001 1001 & 0000 0111 = 0000 0001
    n_y <<= p - 1;              // n_y << 4 = 0001 0000

    x_mask = ~0;                // x_mask = 1111 1111
    x_mask <<= n;               // x_mask << 3 = 1111 1000
    x_mask = ~x_mask;           // ~x_mask = 0000 0111
    x_mask <<= p - 1;           // x_mask << 4 = 0111 0000
    x_mask = ~x_mask;           // ~x_mask = 1000 1111

    n_x = x & x_mask;           // n_x = 1011 1111 & 1000 1111 = 1000 1111

    return n_x | n_y;           // 1000 1111 | 0001 0000 = 1001 1111
*/
