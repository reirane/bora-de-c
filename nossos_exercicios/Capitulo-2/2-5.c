/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
 * The C programming language. 
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 2-5. Write the function any(s1, s2), which returns the first location
 * in the string s1 where any character from the string s2 occurs, or -1 if s1
 * contains no characters from s2. (The standard library function strpbrk does
 * the same job but returns a pointer to the location.) 
 * 
 * reirane e ewerton
 * 19.06.2020
 * 
 * input: ""
 * 
 * output: "2"
 * 
 */

#include<stdio.h>

int any(char[], char[]);

int main()
{
    printf("%d\n", any("bcedfgh", "aeiou"));
    
    return 0;
}

int any(char s1[], char s2[])
{
    int index_s1, index_s2;
    int result = -1;
    
    for(index_s1 = 0; s1[index_s1] != '\0' ; index_s1++)
        for(index_s2 = 0; s2[index_s2] != '\0'; index_s2++)
            if (s1[index_s1] == s2[index_s2] && result == -1)
                result = index_s1;
    
    return result;
}
