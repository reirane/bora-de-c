/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
 * The C programming language. 
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 2-9. In a two's complement number system, x &= (x-1) deletes the
 * rightmost 1-bit in x. Explain why. Use this observation to write a faster
 * version of bitcount.
 * 
 * reirane e ewerton
 * 28.07.2020
 * 
 * input: ""
 * 
 * output: "1"
 *         "1"
 *         "3"
 *         "3"
 *         "4"
 *         "4"
 *         "7"
 *         "7"
 * 
 */

#include<stdio.h>

/* bitcount: count 1 bits in x */
int bitcount_e (unsigned);
int bitcount_r (unsigned);

int main()
{
    printf("%d\n", bitcount_e(1));
    printf("%d\n", bitcount_r(1));
    printf("%d\n", bitcount_e(7));
    printf("%d\n", bitcount_r(7));
    printf("%d\n", bitcount_e(57));
    printf("%d\n", bitcount_r(57));
    printf("%d\n", bitcount_e(127));
    printf("%d\n", bitcount_r(127));
}

/* In x &= (x - 1), the rightmost bit evaluated as 1 will ever
   be AND operated with its opposite. Check the example below:
   
   0011 1001	(57)
 & 0011 1000	(56)
   ---------
   0011 1000	(56)


   0011 1000	(56)
 & 0011 0111	(55)
   ---------
   0011 0000	(48)

	
   0011 0000	(48)
 & 0010 1111	(47)
   ---------
   0010 0000	(32)
	
	
   0010 0000	(32)
 & 0001 1111	(31)
   ---------
   0000 0000	(0)
   
   It happens because of the 'minus one' and the properties of
   two's complement. This way, all 1s will be zeroed and we can
   count how many times we had to iterate the loop to achieve
   the 0 value.
   
   For more information about two's complement check:
   https://www.cs.cornell.edu/~tomf/notes/cps104/twoscomp.html
*/
int bitcount_e (unsigned x)
{
    int b;
    
    for (b = 0; x > 0; x &= (x - 1))
        b++;
    
    return b;
}

/*
   Todas as vezes que extraimos 1 de um dado valor em base 2, 
   descompletamos um conjunto de elementos e, por conseguinte, 
   mudamos o valor da casa decimal mais à direita, da qual 
   será subtraído o valor.

   Fato interessante é, dado que a representação binária 
   apenas abarca dois algarismos, (0 e 1), a quantidade de 
   algarismos 1 corresponde à quantidade de subtrações 
   necessárias para tornar zero o valor do número binário.
*/
int bitcount_r (unsigned x)
{
    int b;
    b = 0;
    
    while (x > 0) {
        x &= x-1;
        b++;
    }
    
    return b;
}

