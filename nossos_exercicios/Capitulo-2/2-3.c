/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
 * The C programming language. 
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 2-3. Write the function htoi(s), which converts a string of
 * hexadecimal digits (including an optional 0x or 0X) into its equivalentinteger value.
 * The allowable digits are 0 through 9, a through f, and A through F.
 * 
 * reirane e ewerton
 * 22.06.2020
 * 
 * input: ""
 * 
 * output: "175"
 * 
 */

#include <stdio.h> 
#include <ctype.h>
#include <stdlib.h>
#define interval 7 
#define contagem 16

int power (int, int);
int htoi_r (const char[]);
int htoi_e (const char[]);

int main()
{
    char *hex = "0xaf";
    
    printf("%d\n", htoi_r(hex));
    printf("%d\n", htoi_e(hex));
    
    return 0;
}

int htoi_r (const char hexa[])
{
    int result = 0;
    int decimal;
    int indice = 0;
    int potencia;
    
    for(indice; hexa[indice] != '\0'; indice++)
        ;
    
    for(indice = indice - 1, potencia = 0; indice >= 0 && hexa[indice] != 'x'; indice--, potencia++){
        decimal = toupper(hexa[indice]) - '0' - interval;
        result += decimal * power(contagem, potencia);
    }
    
    return result;
}

/*power: raise base to n-th power; n >= 0  - disponivel na pagina 24 do livro*/
int power(int base, int n)
{
    int i, p;

    p = 1;
    for (i = 1; i <= n; ++i)
        p *= base;
    
    return p;
}

int htoi_e (const char hex[])
{
    int counter = 0;
    int multiplier = 0;
    int len = 0;
    int hasPrefix = 0;

    for (int i = 0; hex[i] != '\0'; i++) {
        /*
        if the string starts with '0x', increase the loop iterator
        in two positions ('0', 'x') and set hasPrefix.
        */
        if (i == 0 && hex[0] == '0' && toupper(hex[1]) == 'X') {
            i = 2;
            hasPrefix = 1;
        }

        // len will have the real length on the hexa string
        len++;
    }

    /*
    multiplier will be used to compute the power of 16.
    As the power goes from n - 1 to 0, we must decrease
    one of the real length of the hexa string.
    */
    multiplier = len - 1;

    /*
    if hasPrefix is set, then start to count from index 2, if not,
    start from 0.
    */
    for (int i = hasPrefix ? 2 : 0; hex[i] != '\0'; i++) {
        if (isdigit(hex[i]))
            /*
            (hex[i] - '0') gives us the value of the char as an int when it's
            a digit.

            if --multiplier is greater than zero, we can use the shift left to
            compute the power of 16, if not, we have to use the shift right
            with the absolute value of the multiplier  We do that because
            bitwise operations with negative values return warnings.

            16 << 4 == 16 ^ 2
            16 << 0 == 16 ^ 1
            16 << -4 -> 16 >> 4 == 16 ^ 0
            */
            if (--multiplier >= 0)
                counter += (toupper(hex[i]) - '0') * (16 << (4 * multiplier));

            else
                counter += (toupper(hex[i]) - '0') * (16 >> (4 * abs(multiplier)));

        else {
            /*
            (toupper(hex[i]) - '0' - 7) is used because there are seven characters
            between the '9' and the 'A', they're ':', ';', '<', '=', '>', '?' and '@'.
                                                  1    2    3    4    5    6       7
            */
            if (--multiplier >= 0)
                counter += (toupper(hex[i]) - '0' - 7) * (16 << (4 * multiplier));

            else
                counter += (toupper(hex[i]) - '0' - 7) * (16 >> (4 * abs(multiplier)));
        }
    }

    return counter;
}

