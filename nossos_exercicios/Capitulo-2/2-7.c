/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E.
 * The C programming language.
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 2-7. Write a function invert (x, p, n) that returns x with the n bits
 * that begin at position p inverted (i.e., 1 changed into 0 and vice versa), leaving
 * the others unchanged.
 *
 * reirane e ewerton
 * 13.07.2020
 *
 * input: ""
 *
 * output: "211"
 *         "141"
 *         "163"
 *         "211"
 *         "141"
 *         "163"
 *
 */

#include<stdio.h>

unsigned invert_e (unsigned, int, int);
unsigned invert_r (unsigned, int, int);

int main()
{
    printf("%u\n", invert_e(0b10101011, 7, 4));
    printf("%u\n", invert_e(0b10010101, 5, 2));
    printf("%u\n", invert_e(191, 5, 3));
    printf("%u\n", invert_r(0b10101011, 7, 4));
    printf("%u\n", invert_r(0b10010101, 5, 2));
    printf("%u\n", invert_r(191, 5, 3));
}

unsigned invert_e (unsigned x, int p, int n)
{
    /**/
    int x_mask = ~0;
    x_mask <<= n;
    x_mask = ~x_mask;
    x_mask <<= p - n;
    
    int n_x = x & x_mask;
    n_x = ~n_x;
    
    int n_x_mask = ~0;
    n_x_mask <<= n;
    n_x_mask = ~n_x_mask;
    n_x_mask <<= p - n;
    
    n_x &= n_x_mask;
    
    int x_clean = ~0;
    x_clean <<= n;
    x_clean = ~x_clean;
    x_clean <<= p - n;
    x_clean = ~x_clean;
    x_clean &= x;
    
    return x_clean | n_x;
    
    /** /
    int x_mask = ~(~0 << n) << (p - n);
    
    int n_x = ~(x & x_mask);
    
    int n_x_mask = ~(~0 << n) << (p - n);
    n_x &= n_x_mask;
    
    int x_clean  = x & ~(~(~0 << n) << (p - n));
    
    return x_clean | n_x;
    /**/
    
    /** /
    return x & ~(~(~0 << n) << (p - n)) | ~(x & ~(~0 << n) << (p - n)) & ~(~0 << n) << (p - n);
    /**/
}

unsigned invert_r(unsigned x, int p, int n)
{
    
    return ~(~(~((~(~0 << n)) << p-n) & x) & ~(((~(~0 << n)) << p-n) & ~x));
    
    /*
    // expressão dividida em passos:
    int passo1, passo2, passo3, passo4, passo5, passo6, passo7, passo8;
    
    //zerar bits que quero de x
    passo1 = ~(~0 << n);    
    passo2 = passo1 << p-n; 
    passo3 = ~passo2;       
    passo4 = passo3 & x;    
    
    //isolar bits que quer inverter
    passo5 = ~(~0 << n);        
    passo6 = passo5 << p-n;    
    passo7 = passo6 & ~x;   
    
    passo8 = ~(~passo4 & ~passo7); // espressão final. pode ser alternativamente escrita (passo4 | passo7)
    
    return passo8;
    */
    
}
