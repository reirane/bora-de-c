/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
 * The C programming language. 
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 2-2. Write a loop equivalent to the for loop above
 * without using && or ||.
 * 
 * reirane e ewerton
 * 08.06.2020
 * 
 * input: "abdefghijklmnopqrstuvwxyz"
 * 
 * output: "abdefghijk"
 * 
 */

#include<stdio.h>
#define MAXLINE 10

int main() {
    char s[MAXLINE];
    int c;
    int i;
    
    /*
    for (int i = 0; i < MAXLINE - 1 && (c = getchar()) != '\n' && c != EOF; ++i)
        s[i] = c;
    */
    
    /* This loop does the same that the for loop above does. */
    i = 0;
    while ((c = getchar()) != EOF) {
        if (i > MAXLINE - 1)
            break;

        else if (c != '\n')
            s[i] = c;
            
        else
            s[i] = '\0';

        i++;
    }
    
    /* This loop is similar to the while loop above, but using ternary operator.
       Not sure how to rewrite 'break' statement, so we used the printf("") to do nothing.
       But it doesn't stop the loop, resulting in a waste of resources.
       
    i = 0;
    while ((c = getchar()) != EOF) (i > MAXLINE - 1) ? printf("") : (c != '\n') ? (s[i++] = c) : (s[i++] = '\0');
    */
    
    printf("%s\n", s);
}
