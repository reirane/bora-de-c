/**
* RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
* The C programming language. 
* Englewood Cliffs: Prentice Hall, 1988, 2. ed.
*
* Exercise 2-10. Rewrite the function lower, which converts
* upper case letters to lower case, with a conditional expression
* instead of if-else.
* 
* reirane e ewerton
* 04.08.2020
*/


#include <stdio.h>

/* lower: convert c to lower case; ASCII only */
int lower_r (int);
int lower_e (int);

int main () 
{
    for (int i = 'A'; i <= 'Z'; i++)
        printf("%c: %c | %c\n", i, lower_r(i), lower_e(i));
}

/*Nesta funcao, ficamos nos perguntando porque nao ha necessidade de return. Seria por causa da designacao da expressao para a variavel c?*/
int lower_r (int c)
{
    c = (c >= 'A' && c <= 'Z') ? (c + 'a' - 'A') : c;
}

int lower_e (int c)
{
    return (c >= 'A' && c <= 'Z') ? (c + 'a' - 'A') : c;
}