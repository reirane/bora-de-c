/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E.
 * The C programming language.
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 1-22. Write a program to "fold" long input lines into two or more
 * shorter lines after the last non-blank character that occurs before the n-th
 * column of input. Make sure your program does something intelligent with very
 * long lines, and if there are no blanks or tabs before the specified column.
 *
 * reirane e ewerton
 * 18.05.2020
 *
 * input:   "ABCDEFGH"
 *          "IJKLMNOP"
 *          "QRST"
 *          "UVWXYZ"
 *
 *          "A    BCD EFGH"
 *          "I JKLMNOP"
 *          "QRST"
 *          "UVWXYZ"
 *
 * output:  "ABCDE"
 *          "FGH"
 *          "I"
 *          "JKLMN"
 *          "OP"
 *          "QR"
 *          "ST"
 *          "UV"
 *          "WXYZ"
 *
 *          "A"
 *          "BCD E"
 *          "FGHI"
 *          "JKLMN"
 *          "OPQRS"
 *          "TUVWX"
 *          "YZ"
 *
 */

#include<stdio.h>
#include<stdlib.h>
#define MAXLINE 1000

void readline (char [], int);
char *fold (char [], int);

int main()
{
    char s[MAXLINE];
    int x = 5;

    readline(s, sizeof(s)/sizeof(char));

    printf("%s", fold(s, x));

    return 0;
}

void readline (char phrase[], int limit)
{
    int c, i;

    i = 0;
    while ((c = getchar()) != EOF && i < limit - 1)
        phrase[i++] = c;

    phrase[i] = '\0';
}

char *fold (char phrase[], int intervalo)
{
    int index_s, index_p, count, size;

    for (index_s = 0, size = 0; phrase[index_s] != '\0'; index_s++)
        size++;

    // Arrumar tamanho da alocação da string s
    char *s = malloc(sizeof(*s) * (index_s + size));

    index_s = index_p = count = 0;
    while(phrase[index_p] != '\0') {
        if (phrase[index_p] == '\n')
            s[index_s] = phrase[++index_p];

        if (count < intervalo) {
            if (phrase[index_p] != ' ' && phrase[index_p] != '\t') {
                s[index_s++] = phrase[index_p++];
            }

            else if (count < intervalo - 1) {
                if (phrase[index_p + 1] == ' ' || phrase[index_p + 1] == '\t') {
                    s[index_s + 1] == '\n';
                }

                else {
                    s[index_s++] = phrase[index_p++];
                    count++;
                    continue;
                }

                index_p++;
            }

            count++;
        }

        else {
            if (phrase[index_p] == ' ' || phrase[index_p] == '\t') {
                s[index_s++] = '\n';
                index_p++;
            }

            else {
                s[index_s++] = '\n';
            }

            count = 0;
        }
    }

    return s;
}
