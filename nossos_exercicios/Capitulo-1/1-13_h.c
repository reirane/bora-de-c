/* bora de c - grupo de estudos

RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
The C programming language. 
Englewood Cliffs: Prentice Hall, 1988, 2. ed.

Exercise 1-12. Write a program that prints its input one word per line.

reirane e ewerton
30.04.2020 */

#include <stdio.h>

int main()
{
    int c, i, z;
    
    i = 1;
    z = 0;
    
    while ((c = getchar()) != EOF)
        if (c != ' ' && c != '\n' && c != '\t' && c != '\0') {
            if (z == 0) {
                printf("Tamanho da palavra %2d:\t", i);
                z++;
            }
            
            printf("=");
        }
            
        else {
            printf("\n");
            i++;
            z = 0;
        }
        
    return 0;
}
