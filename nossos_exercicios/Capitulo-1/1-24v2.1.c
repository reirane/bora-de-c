/*Exercise 1-24. Write a program to check a C program for rudimentary syntax
errors like unbalanced parentheses, brackets and braces. Don't forget about
quotes, both single and double, escape sequences, and comments. (This pro-
program is hard if you do it in full generality.)
*/

#include<stdio.h>
#define MAXLINE 5000

void sintaxe (char []);
void readline (char [], int);

int main() {

    char str[MAXLINE];

    readline(str, MAXLINE);

    sintaxe(str);
    
    return 0;
}



void readline (char phrase[], int limit)
{
    int c, i;

    i = 0; //contador do array
    while ((c = getchar()) != EOF && i < limit - 1)
        phrase[i++] = c;

    phrase[i] = '\0';
}


//identifica caracteres desemparelhados
void sintaxe (char phrase[])
{
// contadores para cada tipo de caractere
int i, c, n_parenthesis, n_brackets, n_braces, n_single_quotes, n_double_quotes, n_comment_ast, n_comment_bar;

i = 0;
n_parenthesis = 0;
n_brackets = 0;
n_braces = 0;
n_single_quotes = 0;
n_double_quotes = 0;
n_comment_ast = 0;
n_comment_bar = 0;

while (phrase[i] != '\0'){

    if (phrase[i] == '(')
        n_parenthesis++;

    if (phrase[i] == ')')
        n_parenthesis--;

    if (phrase[i] == '{')
        n_brackets++;

    if (phrase[i] == '}')
        n_brackets--;

    if (phrase[i] == '[')
        n_braces++;

    if (phrase[i] == ']')
        n_braces--;

    if (phrase[i] == '\'')
        n_single_quotes++;

    if (phrase[i] == '\"')
        n_double_quotes++;

    if (phrase[i - 1] == '/' && phrase[i] == '*')
        n_comment_ast++;
    
    if (phrase[i - 1] == '*' && phrase[i] == '/')
        n_comment_ast--;
    
    if (phrase[i - 1] == '/' && phrase[i] == '/')
       n_comment_bar;

    
    i++;

}

printf("You have %d unbalanced parehthesis, %d unbalanced brackets and %d unbalaced braces, %d asterisk comment, %d barr comment \n ", 
        n_parenthesis, n_brackets,  n_braces, n_comment_ast, n_comment_bar);

}

