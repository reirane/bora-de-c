/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
 * The C programming language. 
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 1-21. Write a program entab that replaces strings of blanks by the
 * minimum number of tabs and blanks to achieve the same spacing. Use the
 * same tab stops as for detab. When either a tab or a single blank would suffice
 * to reach a tab stop, which should be given preference?
 * 
 * reirane e ewerton
 * 15.05.2020
 * 
 * input: "A    BB        CCC            DDDD                EE   F end"
 * 
 * output: "A	BB		CCC			DDDD				EE   F end"
 * 
 */

#include<stdio.h>
#define N 4
#define MAX 1000

void entab(char [], int);

int main()
{
    char s[MAX];

    entab(s, MAX);
    
    printf("%s", s);
    
    return 0;
}

void entab(char string[], int limit)
{
    int i, j, c;
    
    i = j = 0;
    while ((c = getchar()) != EOF && i < limit)
        if (c != ' ') {
            string[i++] = c;
            j = 0;
        }
        
        else if (j == N - 1) {
            i -= 3;
            string[i++] = '\t';
            j = 0;
        }
        
        else {
            string[i++] = c;
            j++;
        }
}
