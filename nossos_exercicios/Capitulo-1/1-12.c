/* bora de c - grupo de estudos

RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
The C programming language. 
Englewood Cliffs: Prentice Hall, 1988, 2. ed.

reirane e ewerton
27.04.2020 */

#include <stdio.h>
#define IN 1 /* inside a word */
#define OUT 0 /* outside a word */

/* Exercise 1-12. Write a program that prints its input one word per line. */
int main()
{
    int state, i;
    char str[300];
    
    scanf("%[^EOF]", str);
    state = OUT;
    for (i = 0; i < sizeof(str)/sizeof(char) && str[i] != '\0'; i++)
    {
        if (str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
        {
            printf("%c", str[i]);
            state = IN;
        }
        
        else if (state == OUT)
            continue;
        
        else
        {
            printf("\n");
            state = OUT;
        }
    
    }
    
    return 0;
}