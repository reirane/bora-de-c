/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
 * The C programming language. 
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 1-16 Revise the main routine of the longest-line program so it will
 * correctly print the length of arbitrarily long input lines, and as much as possible of the text
 * 
 * reirane e ewerton
 * 08.05.2020
 * 
 * input: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
 *        bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb
 *        cccccccccc
 *        dddddddddddddddddddddddddddddddddddddddddddddddddd"
 * 
 * output: "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
 * 
 */


#include<stdio.h>
#define MAXLINE 1000 /* maximum input line size */ 

int getLine(char [], int);
void copy(char [], char []);

/* print longest input line */
int main()
{
    int len;    /* current line lenght */
    int max;    /* maximum enght seen so far */
    char line[MAXLINE];     /* current input line */
    char longest[MAXLINE];  /* longest line saved here */
    
    max = 0;
    while ((len = getLine(line, MAXLINE)) > 0)
        if (len > max) {
            max = len;
            copy(longest, line);
        }
        
        if (max > 0)    /* there was a line */
            printf("%s", longest);
        
        return 0;
}

/* getline: read a line into s, return lenght */
int getLine(char s[], int lim)
{
    int c, i;

    for (i = 0; i < lim - 1 && (c=getchar()) != EOF && c != '\n'; ++i)
        s[i] = c;
    
    if (c == '\n') {
        s[i] = c;
        ++i;
    }
    
    s[i] = '\0';
    
    return i;
}

/* copy: copy 'from' into 'to'; assume to is big enough */
void copy(char to[], char from[])
{
    int i;
    
    i = 0;
    while ((to[i] = from[i]) != '\0')
        ++i;
}
