/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
 * The C programming language. 
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 1-18 Write a program to remove trailing blanks and tabs from each
 * line of input, and to delete entirely blank lines.
 * 
 * reirane e ewerton
 * 11.05.2020
 * 
 * input: "asssasas                                          "
 *        "                       aleluia amem a alegria     "
 *        "  esta               no coracao           de quem "
 *        "             ja     conhece                       "
 *        "      a                     jesus"
 * 
 * output: "asssasas aleluia amem a alegria esta no coracao de quem ja conhece a jesus"
 * 
 */

#include<stdio.h>
#define MAXLINE 1000 /* maximum input line size */ 

int getLine(char [], int);

/* print line without trailing spaces */
int main()
{
    char line[MAXLINE];     /* current input line */
    
    while (getLine(line, MAXLINE) > 0)
        printf("%s", line);
    
    return 0;
}

/* getline: read a line into s, return lenght */
int getLine(char s[], int lim)
{
    int c, i, count;
    
    for (i = count = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n';)
        if (c != '\t' && c != ' ') {
            s[i++] = c;
            count = 0;
        }
        
        else {
            count++;
            
            if (count <= 1)
                s[i++] = c;
        }
    
    if (c == '\n')
        s[++i] = c;
    
    s[i] = '\0';
    
    return i;
}

