/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
 * The C programming language. 
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 1-15. Rewrite the temperature conversion program of Section 1.2 to
 * use a function for conversion.
 * 
 * reirane e ewerton
 * 07.05.2020
 */

#include <stdio.h>

/* print Celsius to Fahrenheit table for celsius = 0, 20, ..., 100 */
int conversion (int);

/* chama a função de conversão e imprime tabela */
int main ()
{
    printf("Celsius to Fahrenheit conversion table\n"); // table header
    printf("C\tF\n");
    for (int celsius = 0; celsius <= 100; celsius += 20)
        printf("%d\t%d\n", celsius, conversion(celsius));
    
    return 0;
}

/* funcao que converte celsius para fahrenheit */
int conversion (int celsius)
{
    return (9 * celsius / 5) + 32;
}
