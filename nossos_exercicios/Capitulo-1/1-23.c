/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E.
 * The C programming language.
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 1-23. Write a program to remove all comments from a C program.
 * Don't forget to handle quoted strings and character constants properly.
 * C comments do not nest.
 *
 * adax, reirane e ewerton
 * 25.05.2020
 *
 * input:   (YOU SHOULD REMOVE THE ESCAPE IN THE COMMENTS AT LINES 17 AND 23)
 *
 *          "/**"
 *          " * HEADER"
 *          " *"
 *          " *\/"
 *          ""
 *          "// main declaration"
 *          "int main (void);"
 *          ""
 *          "/* main implementation *\/"
 *          "int main ()"
 *          "{"
 *          "       printf("Hello, World!");"
 *          "       printf("/*");"
 *          "       printf("\/\*");"
 *          "}"
 *          ""
 *
 * output:  ""
 *          "int main (void);"
 *          ""
 *          ""
 *          "int main ()"
 *          "{"
 *          "	printf("Hello, World!");"
 *          "	printf("/*");"
 *          "	printf("\/\*");"
 *          "}"
 *          ""
 *
 */

#include<stdio.h>
#include<stdlib.h>
#define MAXLINE 1000

void readline (char [], int);
void uncomment (char []);

int main()
{
    char str[MAXLINE];

    readline(str, sizeof(str)/sizeof(char));

    uncomment(str);

    return 0;
}

void readline (char phrase[], int limit)
{
    int c, i;

    i = 0; //contador do array
    while ((c = getchar()) != EOF && i < limit - 1)
        phrase[i++] = c;

    phrase[i] = '\0';
}

void uncomment (char program[])
{
    int i, escaped, c_slash, c_asterisk;

    for (i = c_slash = escaped = c_asterisk = 0; program[i] != '\0'; i++) {
        if (c_asterisk || c_slash) {
            if (c_asterisk && program[i] == '*' && program[i + 1] == '/') {
                c_asterisk = 0;
                i++;
            }

            if (c_slash && program[i] == '\n')
                c_slash = 0;
        }

        else {
            if (program[i] == '\'' || program[i] == '"' || program[i] == '\\') {
                if (escaped)
                    escaped = 0;

                else
                    escaped = 1;
            }

            else if (!escaped && program[i] == '/' && program[i + 1] == '*') {
                c_asterisk = 1;
                i++;
                continue;
            }

            else if (!escaped && program[i] == '/' && program[i + 1] == '/') {
                c_slash = 1;
                i++;
                continue;
            }

            printf("%c", program[i]);
        }
    }
}
