/* bora de c - grupo de estudos

RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
The C programming language. 
Englewood Cliffs: Prentice Hall, 1988, 2. ed.

Exercise 1-13-Vertical. Write a program to print a histogram of the lengths of words in
its input. It is easy to draw the histogram with the bars horizontal; a vertical
orientation is more challenging.

reirane e ewerton
01.05.2020 */

//input: a1 bb22 ccc333 dddd4444 eeeee55555 ffffff666666 12341234

#include<stdio.h>
#define MAX 300

int main()
{
    int c, i, biggest, biggest_aux, linha, coluna;
    int len[MAX];
    
    for (i = 0; i < sizeof(len)/sizeof(int); i++)
        len[i] = -1;
    
    biggest = i = 0;
    while ((c = getchar()) != EOF)
        if (c != ' ' && c != '\n' && c != '\t' && c != '\0')
            if (len[i] == -1)
                len[i] = 1;

            else
                len[i]++;

        else {
            if (biggest < len[i])
                biggest = len[i];
            
            i++;
        }
    
    for (linha = 0, biggest_aux = biggest; linha < biggest; linha++) {
        for (coluna = 0; coluna < sizeof(len)/sizeof(int) && len[coluna] != -1; coluna++)
            //printf("[%2d][%2d] = %2d:%2d\t", linha, coluna, len[coluna], biggest_aux);  // LINHA DE TESTE PARA VISUALIZAR
                                                                                          // A MATRIZ COM OS VALORES DOS INDICES
            if (len[coluna] == biggest_aux) {
                printf("| ");
                len[coluna]--;
            }
            
            else
                printf(". ");
        
        printf("\n");
        biggest_aux--;
    }
    
    // Imprimindo indice das palavras
    for (i = 0; i < sizeof(len)/sizeof(int) && len[i] != -1; i++)
        printf("%d ", i + 1);
    
    return 0;

}
