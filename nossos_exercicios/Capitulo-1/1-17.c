/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
 * The C programming language. 
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 1-17 Write a program to print all input lines that are longer than 80
 * characters.
 * 
 * reirane e ewerton
 * 11.05.2020
 * 
 * input: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
 *         bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb
 *         ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc"
 * 
 * output: "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb
 *          ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc"
 * 
 */

#include<stdio.h>
#define MAXLINE 1000 /* maximum input line size */ 

int getLine(char [], int);

/* print lines greater than 80 characters */
int main()
{
    int len;                /* current line lenght */
    char line[MAXLINE];     /* current input line */
    
    while ((len = getLine(line, MAXLINE)) > 0)
        if (len > 80)
            printf("%s", line);

    return 0;
}

/* getline: read a line into s, return lenght */
int getLine(char s[], int lim)
{
    int c, i;

    for (i = 0; i < lim - 1 && (c=getchar()) != EOF && c != '\n'; ++i)
        s[i] = c;
    
    if (c == '\n') {
        s[i] = c;
        ++i;
    }
    
    s[i] = '\0';
    
    return i;
}
