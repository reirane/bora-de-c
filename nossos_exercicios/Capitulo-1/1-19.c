/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
 * The C programming language. 
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 1-16 Write a function reverse(s) that reverses the character
 * string s. Use it to write a program that reverses its input a line at a time.
 * 
 * reirane e ewerton
 * 13.05.2020
 * 
 * input: "ABCDE"
 * 
 * output: "EDCBA"
 * 
 */

#include<stdio.h>
#include<stdlib.h>
#define MAXLINE 1000 /* maximum input line size */ 

int getLine(char [], int);
char *reverse(char []);

/* print reversed input line */
int main()
{
    char line[MAXLINE];     /* current input line */
    
    while (getLine(line, MAXLINE) > 0)
            printf("%s", reverse(line));
    
    return 0;
}

/* reverse string */
char *reverse(char string[])
{
    int i, j, count;
    
    i = count = 0;
    while (string[i++] != '\n')
        count++;
    
    /*
        Based on the input given on header, at this point, the values 
        of the variables i and count are, respectively, 6 and 5.
        Test line below.
    */
    //printf("%d - %d\n=====\n", i, count);
    
    /*
        Here we're allocating memory on heap. We do this because we lost
        the pointer *s when we got out to the function caller, i.e., the
        pointer *s doesn't exist outside the *reverse(char []) function.
        
        Putting it on heap, we can access it when we go back to the caller.
    */
    char *s = malloc(sizeof(*s) * count);

    /*
        Again, for the input given as example, we have that following situation:
            string[0] = A
            string[1] = B
            string[2] = C
            string[3] = D
            string[4] = E
            string[5] = \n
        
        So, we have to initialize the decreasing counter in the last valid
        char (E), not in the last char (\n). Remember that count is 5, that's
        why we subtract 1 from it.
    */
    for (i = 0, j = count - 1; i < count; i++, j--) {
        s[i] = string[j];
        
        /*
            Now we have this behavior:
                s[0] = A	string[4] = A
                s[1] = B	string[3] = B
                s[2] = C	string[2] = C
                s[3] = D	string[1] = D
                s[4] = E	string[0] = E
            
            Test line below.
        */
        //printf("s[%d] = %c\tstring[%d] = %c\n", i, s[i], j, string[j]);
    }
    
    /*
        After all this, remember to copy the \n to the end of the string.
    */
    s[count] = string[count];
    
    return s;
}

/* getline: read a line into s, return length */
int getLine(char s[], int lim)
{
    int c, i;

    for (i = 0; i < lim - 1 && (c=getchar()) != EOF && c != '\n'; ++i)
        s[i] = c;
    
    if (c == '\n') {
        s[i] = c;
        ++i;
    }
    
    s[i] = '\0';
    
    return i;
}
