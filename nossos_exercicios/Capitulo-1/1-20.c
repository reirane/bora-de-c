/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
 * The C programming language. 
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 1-20. Write a program detab that replaces tabs in the input with the
 * proper number of blanks to space to the next tab stop. Assume a fixed set of
 * tab stops, say every n columns. Should n be a variable or a symbolic parameter?
 * 
 * reirane e ewerton
 * 14.05.2020
 * 
 * input: "A	BB		CCC			DDDD				end"
 * 
 * output: "A    BB        CCC            DDDD                end"
 * 
 */

#include<stdio.h>
#define N 4
#define MAX 1000

void detab(char [], int);

int main()
{
    char s[MAX];
    
    detab(s, MAX);
    
    printf("%s", s);
    
    return 0;
}

void detab(char string[], int limit)
{
    int i, j, c;

    
    i = j = 0;
    while ((c = getchar()) != EOF && i < limit) {
        if (c != '\t')
            string[i++] = c;
        
        else
            while (j < N) {
                string[i++] = ' ';
                j++;
            }
            
        j = 0;
    }
}

