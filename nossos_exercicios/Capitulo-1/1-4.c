#include <stdio.h>

/*print Celsius to Fahrenheit table for celsius = 0, 20, ..., 100 */


int main ()
{
	int celsius, fahr;
	int lower, upper, step;

	lower = 0;    /* lower limit of temperature table */
	upper = 100;  /* upper limit of temperature table */
	step = 20;    /* step size */

	printf("Celsius to Fahrenheit conversion table\n"); /*i've included two headers to 
							    get a more readable output*/
	printf("C\tF\n");

	celsius = lower;
	while (celsius <= upper) {
		fahr = (9 * celsius / 5) + 32;
		printf("%3d %6d\n", celsius, fahr);
		celsius = celsius + step;
	}
}
