/* bora de c - grupo de estudos

RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
The C programming language. 
Englewood Cliffs: Prentice Hall, 1988, 2. ed.

Exercise 1-14. Write a program to print a histogram of the
frequencies of different characters in its input.

reirane e ewerton
05.05.2020 */

#include<stdio.h>
#include<ctype.h>

int main()
{
    int c, i, z;
    int quant[26];
    
    i = 0;
    z = 0;
    
    for (i = 0; i < sizeof(quant)/sizeof(int); i++)
        quant[i] = 0;
    
    while ((c = tolower(getchar())) != EOF) // trata letras maiusculas
        if (c >= 'a' && c <= 'z')
            ++quant[c - 'a'];
        
    for (i = 0; i < sizeof(quant)/sizeof(int); i++) {
        printf("%c:\t", i + 'a');
        
        for(z = 0; z < quant[i]; z++)
            printf("=");
        
        printf("\n");
    }
}
