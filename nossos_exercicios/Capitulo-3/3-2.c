/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E.
 * The C programming language.
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 3-2. Write a function escape (s,t) that converts characters like
 * newline and tab into visible escape sequences like \n and \t as it copies the
 * string t to s. Use a switch. Write a function for the other direction as well,
 * converting escape sequences into the real characters.
 *
 * reirane e ewerton
 * 25.08.2020
 *
 * input:   "a	b		c"
 *          "def"
 *          "gh"
 *
 * output:  "a\tb\t\tc\ndef\ngh\n"
 *          "a	b		c"
 *          "def"
 *          "gh"
 *
 */

#include<stdio.h>
#include<stdlib.h>
#define MAXLINE 100

void escape (char[], char[]);
void escape_reverse (char[], char[]);
void readline (char[], int);

int main()
{
    char source[MAXLINE];
    char destination[2 * (sizeof(source)/sizeof(char))];
    
    readline(source, sizeof(source)/sizeof(char));
    
    escape(destination, source);
    printf("%s\n", destination);
    
    escape_reverse(destination, source);
    printf("%s\n", destination);
    
}

void readline (char phrase[], int limit)
{
    int c, i;

    i = 0;
    while ((c = getchar()) != EOF && i < limit - 1)
        phrase[i++] = c;

    phrase[i] = '\0';
}

void escape (char s[], char t[])
{
    int index_t, index_s;
    
    for (index_t = index_s = 0; t[index_t] != '\0'; index_t++)
    {
        switch (t[index_t]) {
            default:
                s[index_s++] = t[index_t];
                break;
            
            case '\t':
                s[index_s++] = '\\';
                s[index_s++] = 't';
                break;
            
            case '\n':
                s[index_s++] = '\\';
                s[index_s++] = 'n';
                break;
        }
    }
    
    s[index_s] = '\0';
}


void escape_reverse (char s[], char t[])
{
    int index_t, index_s;
    
    for (index_t = index_s = 0; t[index_t] != '\0'; index_t++)
    {
        switch (t[index_t]) {
            default:
                s[index_s++] = t[index_t];
                break;
            
            case 't':
                if (t[--index_t] == '\\')
                    s[index_s++] = '\t';
                break;
            
            case 'n':
                if (t[--index_t] == '\\')
                    s[index_s++] = '\n';
                break;
        }
    }
    
    s[index_s] = '\0';
}
