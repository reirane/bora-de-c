/**
 * RITCHIE, Dennis M.; KERNIGHAN, Brian W.; LESK, Michael E. 
 * The C programming language. 
 * Englewood Cliffs: Prentice Hall, 1988, 2. ed.
 *
 * Exercise 3-1. Our binary search makes two tests inside the loop, when one
 * would suffice (at the price of more tests outside). Write a version with only one
 * test inside the loop and measure the difference in run-time.
 * 
 * reirane e ewerton
 * 06.08.2020
 * 
 * input: ""
 * 
 * output: "5 | 5 | 5"
 * 
 * Como resposta ao exercicio, é curioso notar que a diferença de tempo entre as implementacoes hoje em dia é paticamente 
 * inexiste. Como exercicio posterior para o fim do ano, vamos tentar fazer uma comparacao com uma maquina da época para sabermos se
 * em 1978 fazia diferença.
 *
 * As an answer to the exercise, it is curious to notice that the timestamp diference between implementations nowdays is barely
 * noticiable. As further exercise, we will try to compare performances of a machine from 1978 and a current machine to understand if
 * there was a difference.     
 */


#include<stdio.h>
#include<time.h>
#define ARRAY_SIZE 10
#define BILLION 1000000000.0

/* binsearch: find x in v[0] <= v[1] <= ... <= v[n-1] */
int binsearch(int, int[], int);
int binsearch_improved_r(int, int[], int);
int binsearch_improved_e(int, int[], int);

int main()
{
    int array[] = {
        107, 199, 223, 231, 247,
        678, 699, 766, 819, 918
    };
    
    int x = array[5];
    
    printf("%d | %d | %d\n", binsearch(x, array, ARRAY_SIZE), binsearch_improved_r(x, array, ARRAY_SIZE), binsearch_improved_e(x, array, ARRAY_SIZE));
}


/* Copiado do exemplo no livro do link https://www.ime.usp.br/~pf/algoritmos/aulas/bubi.html */
int binsearch_improved_r(int x, int vetor[], int n)
{
    int extrema_direita, extrema_esquerda, ponto_medio;
    struct timespec start, end;
    
    extrema_esquerda = -1;
    extrema_direita = n;
    
    clock_gettime(CLOCK_REALTIME, &start);
    while (extrema_esquerda < extrema_direita - 1)
    {
        ponto_medio = (extrema_esquerda + extrema_direita) / 2;
        if (x > vetor[ponto_medio])
            extrema_esquerda = ponto_medio;
        else
            extrema_direita = ponto_medio;
    }
    clock_gettime(CLOCK_REALTIME, &end);
    printf("[!] Execution time of binsearch_improved_r: %d ns\n", (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / BILLION);
    
    return extrema_direita;
}

int binsearch_improved_e(int x, int v[], int n)
{
    int low, high, mid;
    struct timespec start, end;

    low = 0;
    high = n - 1;

    clock_gettime(CLOCK_REALTIME, &start);
    while (low <= high) {
        mid = (low + high) / 2;
        if (x <= v[mid])
            high = mid - 1;
        else
            low = mid + 1;
    }
    clock_gettime(CLOCK_REALTIME, &end);
    printf("[!] Execution time of binsearch_improved_e: %d ns\n", (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / BILLION);

    return mid;
}

int binsearch(int x, int v[], int n)
{
    int low, high, mid;
    struct timespec start, end;

    low = 0;
    high = n - 1;

    clock_gettime(CLOCK_REALTIME, &start);
    while (low <= high) {
        mid = (low+high) / 2;
        if (x < v[mid])
            high = mid - 1;
        else if (x > v[mid])
            low = mid + 1;
        else {   /* found match */
            clock_gettime(CLOCK_REALTIME, &end);
            printf("[!] Execution time of binsearch: %d ns\n", (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / BILLION);

            return mid;
        }
    }
    clock_gettime(CLOCK_REALTIME, &end);
    printf("[!] Execution time of binsearch: %d ns\n", (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / BILLION);

    return -1; /* no match */
}

