# bora de c

Grupo de estudos da linguagem C. A cada encontro, resolvemos um exercício ou lemos um tópico do livro The C Programming Language, de Brian Kernighan e Dennis Ritchie. O clássico manual da liguagem, escrito em 1978 e reeditado em 1988 foi escolhido depois de tentarmos estudar por algumas fontes online e percebermos inconsistências nos conteúdos sistematizados.

Encontros todos os dias pelas manhãs.