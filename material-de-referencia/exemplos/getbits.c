#include<stdio.h>

unsigned getbits(unsigned, int, int);

int main()
{
    unsigned int a = 0b11111111;
    int p = 7;
    int n = 3;
    
    printf("%u\n", getbits(a, p, n));
}

/* getbits: get n bits from position p */
unsigned getbits(unsigned x, int p, int n)
{
             // p = 7, n = 3
    int step0 = p + 1 - n;          // 7 + 1 - 3 = 5
    int step1 = x >> step0;         // 1111 1111 >> 5 = 0000 0111
                                    // (1111 1111 -> 0111 1111 -> 
                                    //     0011 1111 -> 0001 1111 ->
                                    //     0000 1111 -> 0000 0111)
    
    int step2 = ~0;                 // ~0000 0000 = 1111 1111
    int step3 = step2 << n;         // 1111 1111 << 3 = 1111 1000
    int step4 = ~step3;             // ~1111 1000 = 0000 0111
    
    int step5 = step1 & step4;      // 0000 0111 & 0000 0111 = 0000 0111
    
    printf("step0 = %d\nstep1 = %d\nstep2 = %d\n"
           "step3 = %d\nstep4 = %d\nstep5 = %d\n",
           step0, step1, step2, step3, step4, step5);
    
    return (x >> (p + 1 - n)) & ~(~0 << n);
}

