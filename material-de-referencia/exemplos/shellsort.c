#include <stdio.h>
#include <stdlib.h>

void shellsort(int[], int);
void printArray (int[], int);

int main()
{
    int a[] = {
        5, 50, 46, 55, 33,
        18, 86, 23, 28, 36,
        41, 4, 0, 80, 2,
        37, 88, 92, 87, 61,
        8, 89, 7, 6, 84,
        100, 32, 77, 59, 51,
        43, 38, 74, 52, 34,
        95, 1, 16, 65, 58,
        9, 63, 78, 21, 79,
        10, 64, 97, 82, 25
    };

    printArray(a, sizeof(a)/sizeof(int));

    shellsort(a, sizeof(a)/sizeof(int));

    printf("\n\n");

    printArray(a, sizeof(a)/sizeof(int));

    return 0;
}

void shellsort(int v[], int n)
{
    int gap, i, j, temp;

    for (gap = n / 2; gap > 0; gap /= 2) {
        for (i = gap; i < n; i++)
            for (j = i - gap; j >= 0 && v[j] > v[j + gap]; j -= gap)
            {
                temp = v[j];
                v[j] = v[j + gap];
                v[j + gap] = temp;
            }

        /** / // Call to printArray to help to understand each step
        printf("\n\n[!] gap: %d\n", gap);
        printArray(v, n); // */
    }
}

void printArray (int arr[], int n)
{
    for (int i = 0; i < n; i++)
        if (i > 0 && !(i % 5))
            printf("\n%2d ", arr[i]);
        else
            printf("%2d ", arr[i]);
}

